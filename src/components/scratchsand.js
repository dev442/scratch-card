import React, { useRef, useState, useEffect } from "react";
import config from 'visual-config-exposer'
import canvasScratchCard from "canvas-scratch-card";


export default function ScratchCode() {
    
  const ItemArray=config.settings.items
  var length=Object.keys(ItemArray).length-1
  var randNum = Math.floor(Math.random() * (length-0+1)) + 0;
  var  label=Object.entries(ItemArray)[randNum][1].label
  var backImage=Object.entries(ItemArray)[randNum][1].image
  var font=config.settings.titleFont
  const [BackImage, setBackImage]=useState(backImage)
  const ref = useRef(null);
  useEffect(() => {
   console.log(typeof(backImage))
   if(backImage==""){
     setTimeout(() => {
      setBackImage(config.settings.revealImage)
     }, 1000);
     
   }
   
    document.body.style.backgroundImage="url("+config.settings.backgroundImage+")"
document.getElementsByTagName('h1')[0].style.fontFamily=font
document.getElementsByTagName('h1')[0].style.color=config.settings.titleColor
document.getElementsByTagName('h3')[0].style.fontFamily=font
document.getElementsByTagName('h3')[0].style.color=config.settings.labelColor

    var element = document.createElement("link");
        /*var num = Math.random().toFixed(2)*/
        element.setAttribute("rel", "stylesheet");
        element.setAttribute("type", "text/css");
        element.setAttribute("href", "https://fonts.googleapis.com/css?family="+font);
        document.getElementsByTagName("head")[0].appendChild(element)
   
    canvasScratchCard.init({
      dom: ref.current,
     x:20,
     y:20,// radius default 20
      rate: 0.1, // when to call callback 0.1 - 1,
      image: config.settings.frontImage,
     
     
      callback: () => {
          document.getElementById('scratch').style.visibility='hidden'
          document.getElementById('round').style.transform='scale(-1) rotate(180deg) perspective(1500px)';
document.getElementsByTagName('h3')[0].style.visibility='visible'
document.getElementsByTagName('img')[0].style.visibility='visible'
document.getElementsByClassName('App')[0].style.animation='round 1s'
document.getElementById('myCanvas').style.visibility='visible'
document.getElementsByTagName('h3')[0].style.visibility='visible'

        canvasScratchCard.reset();
      }

    });
    
  }, []);
  return (
      <>
      <h1>{config.settings.title}</h1>
    <div className="App">
      
     
      <div
      id='scratch'
        ref={ref}
        style={{
         
          background:"url("+BackImage+")"
        }}
      />
        <h3>{label}</h3> 
        <img  id='round' src={BackImage} alt=''/>
      
    </div>
    
    </>
  );
}
